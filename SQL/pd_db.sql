
CREATE DATABASE pd_db DEFAULT CHARACTER SET utf8;

use pd_db;

/*ユーザー*/
CREATE TABLE user(
	id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user_name varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
	login_id varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
	password varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
	create_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	update_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

/*ソフト*/
CREATE TABLE soft(
	id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	soft_name varchar(256) UNIQUE COLLATE utf8_unicode_ci DEFAULT NULL,
	file_name varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
	soft_info text COLLATE utf8_unicode_ci,
	category_id int(11) COLLATE utf8_unicode_ci DEFAULT NULL,
	create_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	update_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

/*カテゴリ*/
CREATE TABLE category(
	id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	category_name varchar(256) UNIQUE COLLATE utf8_unicode_ci DEFAULT NULL
);

/*レビュー*/
CREATE TABLE review(
	id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	soft_id int(11) NOT NULL,
	user_id int(11) NOT NULL,
	rating int(11)DEFAULT NULL,
	review_title varchar(256) COLLATE utf8_unicode_ci,
	review_text text COLLATE utf8_unicode_ci,
	create_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
);
